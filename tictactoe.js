"use strict";

// Put your DOMContentLoaded event listener here first.
document.addEventListener("DOMContentLoaded", function(e) {

    //Creating variables
 
    let winner;
    let curPLayer= document.getElementById("curplayer");
    let CurPlayerHeader = document.querySelector("h2")
    const board = document.querySelector("table");
    const congratsMessage = document.querySelector('aside h2');
    let resetButton = document.getElementById("reset");
    board.addEventListener('click', event => {

        const target = event.target;

        //Checking if the target is a td display "X" or "O"
        if (target.tagName === 'TD') {
            if (target.classList.contains('xsquare') || target.classList.contains('osquare')) {
                return;
            } 
            else if (winner === "X" || winner === "O"){
                return;
            }
            else {
              target.textContent = curPLayer.textContent;
              if(curPLayer.textContent === "X"){
                target.classList.add("xsquare");
                curPLayer.textContent = "O";
                CurPlayerHeader.style.color = 'red';
            
              }
              else if(curPLayer.textContent === "O"){
                target.classList.add("osquare");
                curPLayer.textContent = "X";
                CurPlayerHeader.style.color = 'rgb(43, 144, 207)'; 
                
              }
              
            }

        }
        //Populate positions array with current values on board

        const positions = [];

        const tdElements = board.querySelectorAll('td');

        for (let i =0;i<tdElements.length; i++){
            
            
            if (tdElements[i].textContent === "X"){
                positions[i] = "X";
            }
            else if (tdElements[i].textContent === "O"){
                positions[i] = "O";
            }
            else if (tdElements[i].textContent === ""){
                positions[i] = false;
            }
            
            
        }
        
        //Check winner
        winner = checkBoard(...positions);
        if (winner !== false){
            showWinningMessage(winner);
        }
        
      });

    //Function for winning message
    function showWinningMessage(winner){
        let displayedPlayer = document.getElementById("winningplayer");
        displayedPlayer.textContent = winner;
        if (winner === "X"){
            displayedPlayer.style.color='blue';
        }
        else
            displayedPlayer.style.color='red';
        
        congratsMessage.style.visibility = "visible"; 
        CurPlayerHeader.style.visibility = "hidden";
    }
    

    //GAME RESET EVENT -------------

    resetButton.addEventListener('click', ()=>{
        //Reset winner to false
        winner = false;
        //remove all x's and o's on board
        const tdElements = Array.from(board.querySelectorAll('td'));
        for (let i = 0;i<tdElements.length;i++){
            tdElements[i].textContent = "";
            tdElements[i].classList.remove(tdElements[i].classList.item(0));
        }
        //Hide winning message and show player playing message
        congratsMessage.style.visibility = "hidden";
        CurPlayerHeader.style.visibility = "visible";

        //Make current player X
       curPLayer.textContent = "X";
       CurPlayerHeader.style.color = "rgb(43, 144, 207)";
    })
});